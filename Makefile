all:
	ant jar

clean:
	ant clean

ziprun: all
	java -cp out/lib/org.syndicate_lang.actors.jar:out/test/test org.syndicate_lang.actors.example.example2.Main 1000000 200

ziprun1000: all
	java -cp out/lib/org.syndicate_lang.actors.jar:out/test/test org.syndicate_lang.actors.example.example2.Main 1000000 1000
