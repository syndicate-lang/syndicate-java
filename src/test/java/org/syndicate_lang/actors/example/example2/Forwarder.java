package org.syndicate_lang.actors.example.example2;

import org.syndicate_lang.actors.Remote;

public class Forwarder implements IForwarder {
    private final Remote<IForwarder> _main;
    private final int _nRounds;
    private Remote<IForwarder> _peer = null;

    public Forwarder(Remote<IForwarder> main, int nRounds) {
        this._main = main;
        this._nRounds = nRounds;
    }

    @Override
    public void setPeer(Remote<IForwarder> peer) {
        this._peer = peer;
    }

    @Override
    public void handleMessage(final int hopCount) {
        Remote<IForwarder> target = hopCount >= this._nRounds - 1 ? _main : _peer;
        target.async(f -> f.handleMessage(hopCount + 1));
    }
}
