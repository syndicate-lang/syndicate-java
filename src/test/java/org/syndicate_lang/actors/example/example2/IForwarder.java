package org.syndicate_lang.actors.example.example2;

import org.syndicate_lang.actors.Remote;

public interface IForwarder {
    void setPeer(Remote<IForwarder> peer);
    void handleMessage(int hopCount);
}
