package org.syndicate_lang.actors.example.example2;

import org.syndicate_lang.actors.Actor;
import org.syndicate_lang.actors.Remote;

import java.util.ArrayList;
import java.util.List;

import static java.lang.Integer.parseInt;

public class Main implements IForwarder {

    public static void main(String[] args) throws InterruptedException {
        Actor.convenientLogging();
        Actor.forObject(new Main(parseInt(args[0]), parseInt(args[1]))).syncVoid(Main::boot).await();
        Actor.awaitAll();
    }

    public final int _nActors;
    public final int _nRounds;
    public int _remainingToReceive;
    public long _startTime = 0;

    public Main(int nActors, int nRounds) {
        this._nActors = nActors;
        this._nRounds = nRounds;
        this._remainingToReceive = nActors;
    }

    public void boot() {
        Actor.log().info("Available processors: " + Runtime.getRuntime().availableProcessors());
        final List<Remote<IForwarder>> _actors = new ArrayList<>();
        final Remote<IForwarder> me = Actor.ref(this);
        Remote<IForwarder> previous = null;
        for (int i = 0; i < _nActors; i++) {
            Remote<IForwarder> current = Actor.forObject(new Forwarder(me, this._nRounds));
            current.getActor().link();
            _actors.add(current);
            if (previous != null) {
                final var p = previous;
                current.async(f -> f.setPeer(p));
            }
            previous = current;
        }
        _actors.get(0).async(f -> f.setPeer(_actors.get(_nActors - 1)));
        Actor.log().info("Start");
        this._startTime = System.currentTimeMillis();
        _actors.forEach(a -> a.async(f -> f.handleMessage(0)));
    }

    @Override
    public void setPeer(Remote<IForwarder> peer) {
        // Do nothing.
    }

    @Override
    public void handleMessage(int hopCount) {
        this._remainingToReceive--;
        if (this._remainingToReceive == 0) {
            double delta = (System.currentTimeMillis() - this._startTime) / 1000.0;
            long nMessages = (long) _nActors * (long) _nRounds;
            double hz = nMessages / delta;
            Actor.current().stop();
            Actor.log().info(String.format("Stop after %d messages; %.1f seconds, %.1f Hz",
                    nMessages,
                    delta,
                    hz));
        }
    }
}
