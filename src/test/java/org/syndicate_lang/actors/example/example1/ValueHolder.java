package org.syndicate_lang.actors.example.example1;

public class ValueHolder<T> implements IValueHolder<T> {
    private T value;

    public ValueHolder(T initialValue) {
        this.value = initialValue;
    }

    public T get() {
        return this.value;
    }

    public void set(T newValue) {
        this.value = newValue;
    }
}
