package org.syndicate_lang.actors.example.example1;

import org.syndicate_lang.actors.Actor;
import org.syndicate_lang.actors.Remote;

public class Main {
    public static void main(String[] args) throws InterruptedException {
        Actor.convenientLogging();
        final var vh = Actor.forObject(new ValueHolder<>("There"));
        vh.getActor().daemonize();
        final var m = Actor.forObject(new Main());
        m.async(10, (m_) -> m_.run(vh));

        @SuppressWarnings("unchecked")
        IValueHolder<String> vv = vh.syncProxy(IValueHolder.class);

        System.out.println("Value: " + vv.get());
        vv.set("Second");
        System.out.println("Value: " + vv.get());
        System.out.println("Underlying: " + Remote.from(vv));

        Actor.awaitAll();
        System.out.println("Overall main returning");
    }

    private int greetingCounter = 0;

    public void run(Remote<ValueHolder<String>> vh) {
        this.greet((String) vh.syncProxy(IValueHolder.class).get());
        vh.syncVoid((v) -> v.set("World"));
        Actor.current().every(1000, () -> {
            if (greetingCounter >= 3) Actor.current().stop();
            this.greet(vh.sync(ValueHolder::get).await());
        });
    }

    public void greet(String who) {
        Actor.log().info((greetingCounter++) + ": Hi " + who);
    }
}
