package org.syndicate_lang.actors.example.example1;

public interface IValueHolder<T> {
    T get();
    void set(T newValue);
}
