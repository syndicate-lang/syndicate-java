package org.syndicate_lang.actors;

public class ProxyFailure extends RuntimeException {
    public ProxyFailure(Throwable t) {
        super(t);
    }
}
