package org.syndicate_lang.actors;

public class InternalError extends RuntimeException {
    public InternalError(String message, Throwable cause) {
        super(message, cause);
    }
}
