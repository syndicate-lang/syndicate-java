package org.syndicate_lang.actors;

public class ActorTerminated extends Exception {
    private final Actor _actor;

    public ActorTerminated(Actor actor) {
        super("Actor terminated: " + actor, actor.getExitReason());
        this._actor = actor;
    }

    public Actor getActor() {
        return _actor;
    }
}
