package org.syndicate_lang.actors;

import java.util.concurrent.ScheduledFuture;

public class PeriodicTimer {
    private final ScheduledFuture<?> _future;

    protected PeriodicTimer(ScheduledFuture<?> future) {
        this._future = future;
    }

    public boolean isCancelled() {
        return _future.isCancelled();
    }

    public void cancel() {
        _future.cancel(false);
    }
}
