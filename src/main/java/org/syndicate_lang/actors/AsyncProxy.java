package org.syndicate_lang.actors;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class AsyncProxy<T> extends AbstractProxy<T> {
    public AsyncProxy(Remote<T> ref) {
        super(ref);
    }

    @Override
    public Object dispatch(Method method, Object[] args) {
        if (method.getReturnType().equals(void.class)) {
            this._ref.async((v) -> {
                try {
                    method.invoke(v, args);
                } catch (IllegalAccessException e) {
                    throw new ProxyFailure(e);
                } catch (InvocationTargetException e) {
                    throw new ProxyFailure(e.getCause());
                }
            });
            return null;
        } else {
            System.err.println(method.getReturnType());
            throw new UnsupportedOperationException(
                    "Cannot invoke non-void-returning method '" + method + "' asynchronously via AsyncProxy");
        }
    }

    @Override
    boolean isSync() {
        return false;
    }
}
