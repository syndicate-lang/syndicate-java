package org.syndicate_lang.actors;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

public abstract class AbstractProxy<T> implements InvocationHandler {
    protected final Remote<T> _ref;

    public AbstractProxy(Remote<T> ref) {
        this._ref = ref;
    }

    public Remote<T> ref() {
        return this._ref;
    }

    abstract boolean isSync();

    private static Method toStringMethod;

    static {
        try {
            toStringMethod = Object.class.getMethod("toString");
        } catch (NoSuchMethodException e) {
            toStringMethod = null;
        }
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) {
        if (method.equals(toStringMethod)) {
            return this._ref.toString();
        }
        return dispatch(method, args);
    }

    abstract Object dispatch(Method method, Object[] args);
}
