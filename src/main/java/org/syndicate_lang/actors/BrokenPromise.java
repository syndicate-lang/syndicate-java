package org.syndicate_lang.actors;

public class BrokenPromise extends RuntimeException {
    private final Promise<?> _promise;

    public BrokenPromise(Promise<?> promise) {
        super(promise.getReason());
        this._promise = promise;
    }

    public Promise<?> getPromise() {
        return _promise;
    }

    public Throwable getReason() {
        return _promise.getReason();
    }
}
