package org.syndicate_lang.actors;

public interface ThrowingSupplier<T> {
    T get() throws Throwable;
}
