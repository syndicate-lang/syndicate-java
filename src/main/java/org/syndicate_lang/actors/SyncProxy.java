package org.syndicate_lang.actors;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class SyncProxy<T> extends AbstractProxy<T> {
    public SyncProxy(Remote<T> ref) {
        super(ref);
    }

    @Override
    public Object dispatch(Method method, Object[] args) {
        return this._ref.sync((v) -> {
            try {
                return method.invoke(v, args);
            } catch (IllegalAccessException e) {
                throw new ProxyFailure(e);
            } catch (InvocationTargetException e) {
                throw new ProxyFailure(e.getCause());
            }
        }).await();
    }

    @Override
    boolean isSync() {
        return true;
    }
}
